riable "vpc_name" {
  description = "VPC Name"
  type        = string
  default     = "contino"
}

variable "tags" {
  description = "Required Tags to be added to all resources"
  type        = map(any)
  default     = {}
}

variable "vpc_cidr_block" {
  description = "VPC CIDR blocks"
  type        = string
  default     = "10.0.0.0/16"

}

variable "vpc_enable_dns_support" {
  description = "VPC Enable DNS support"
  type        = bool
  default     = true
}

variable "vpc_enable_dns_hostnames" {
  description = "VPC Enable DNS hostnames"
  type        = bool
  default     = true
}

variable "subnets" {
  description = "List of subnets to create"
  type        = list(string)
}

variable "azs" {
  description = "availablity zone to attach subnet"
  type        = list(string)
}

