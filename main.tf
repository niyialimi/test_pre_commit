form {
  required_version = "~> 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.31.0"
    }
  }
}

# Creating VPC
resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = var.vpc_enable_dns_support
  enable_dns_hostnames = var.vpc_enable_dns_hostnames
  tags = merge(
    {
      "Name" = var.vpc_name
    },
    var.tags
  )
}

resource "aws_flow_log" "this" {
  iam_role_arn    = aws_iam_role.this.arn
  log_destination = aws_cloudwatch_log_group.this.arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.this.id
}

resource "aws_cloudwatch_log_group" "this" {
  name              = "example"
  retention_in_days = 90
  kms_key_id        = "someKey"
}

resource "aws_iam_role" "this" {
  name = "example"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "this" {
  name = "example"
  role = aws_iam_role.this.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "aws_s3_bucket.example.arn"
    }
  ]
}
EOF
}

# Creating Subnets - looping through the list of subnets and availablity zones
resource "aws_subnet" "this" {
  count = length(var.subnets)

  vpc_id            = aws_vpc.this.id
  cidr_block        = element(var.subnets, count.index)
  availability_zone = element(var.azs, count.index)

  tags = merge(var.tags, { "Name" = "${var.vpc_name}-${element(var.azs, count.index)}" })
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.this.id
}

